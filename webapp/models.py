from django.db import models


class UrlsId(models.Model):
    url = models.TextField(max_length=1000)
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return self.url

    def __int__(self):
        return self.id
