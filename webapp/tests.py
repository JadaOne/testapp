from django.test.client import Client
import json
from django.test import TestCase


class TestClass(TestCase):

    def test_01(self, url={'url': 'https://habrahabr.ru/post/122156/'}):   # post url
        c = Client()
        data = json.dumps(url)
        response = c.post('/endpoint/tags/', data, content_type="application/json")
        print(response.status_code)
        return response.content  # returns id

    def test_02(self, id=3):  # get request by id
        c = Client()
        url = '/endpoint/tags/' + str(id)
        response = c.get(url)
        print(response.status_code)
        print(response.content)

    def test_03(self):
        valid_url = {'url': 'https://habrahabr.ru/post/122156/'}
        invalid_url = {'url': 'ksjdhfjshdfkjh'}
        valid_id = self.test_01(valid_url)
        invalid_id = self.test_01(invalid_url)
        self.test_02(valid_id.decode('utf-8'))
        self.test_02(invalid_id.decode('utf-8'))
        self.test_02(100000)


