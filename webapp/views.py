from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from . import fncs


def give_data(request, url_id):
    url = fncs.get_url_from_file(url_id)
    if url is None:
        return HttpResponse('Url with this id does not exist')
    return HttpResponse(fncs.parse_html(url))


@csrf_exempt
def get_url(request):
    if request.method == 'POST':
        url = json.loads(request.body).values()
        return HttpResponse(fncs.get_url_id(url))


