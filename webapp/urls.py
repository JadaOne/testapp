from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^endpoint/tags/(?P<url_id>\d+)', views.give_data, name='give_data'),
    url(r'^endpoint/tags/', views.get_url, name='get_data'),
]
