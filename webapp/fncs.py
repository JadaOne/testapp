import requests
from lxml import etree
from io import StringIO
import json
from . import models


def do_smthng_great(root, d):
    if root.tag in d:
        d[root.tag] = d[root.tag] + 1
    else:
        d.update({root.tag: 1})
    for row in root:
        do_smthng_great(row, d)


def parse_html(url):
    # r = None
    try:
        r = requests.get(url)
    except Exception:
        return 'Invalid url'

    if r.headers['content-type'].find('html') is None:
        return 'Type is not html'

    parser = etree.HTMLParser(encoding='utf-8', remove_comments=True)
    tree = etree.parse(StringIO(r.text), parser)

    d = dict()
    try:
        do_smthng_great(tree.getroot(), d)
    except Exception:
        return 'Error while parsing'

    return json.dumps(d)


def get_url_from_file(id):

    f = models.UrlsId.objects.filter(id=str(id)).first()
    if f is not None:
        return f.url[14:-3]
    else:
        return None


def get_url_id(url):
    f = models.UrlsId.objects.filter(url=url).first()
    if f is not None:
        return f.id
    else:
        f = models.UrlsId(url=url)
        f.save()
        return f.id

